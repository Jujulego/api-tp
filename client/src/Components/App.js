import React, { Component } from 'react';

import ListeFilms from "./ListeFilms";

class App extends Component {
    render() {
        return (
            <div className="bg-light">
                <ListeFilms />
            </div>
        );
    }
}

export default App;
