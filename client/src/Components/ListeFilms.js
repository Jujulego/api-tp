import React, { Component } from 'react';
import axios from 'axios';

import { Container, Row, Col, Navbar, NavbarBrand, NavItem, NavLink, Nav } from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Film from "./Film";

import './ListeFilms.css';

class ListeFilms extends Component {
    constructor(props) {
        super(props);

        this.state = {
            films: [],
            loading: true,
        }
    }

    componentDidMount() {
        this.requestContent();
    }

    render() {
        // Rendu !
        const films = [];
        for (const film of this.state.films) {
            films.push(
                <Film key={ film.id } film={ film } />
            )
        }

        return (
            <div>
                <Navbar className="text-light" color="dark" dark>
                    <NavbarBrand>Films</NavbarBrand>
                    <Nav navbar>
                        <NavItem>
                            <NavLink onClick={ () => { this.requestContent() } }>
                                <FontAwesomeIcon icon="sync-alt" spin={ this.state.loading } />
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>

                <Container className="ListeFilms p-3" fluid>
                    <Row>
                        <Col>
                            <div className="films-container">{ films }</div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

    requestContent() {
        this.setState({ loading: true });

        // Récupération des films
        axios.get("http://localhost:3030/api/movies")
            .then((rep) => this.setState({ films: rep.data, loading: false }))
            .catch((err) => {
                console.error("Request failed !", err);
                this.setState({ loading: false });
            })
    }
}

export default ListeFilms;