import React, { Component } from 'react';
import { Card, CardHeader, CardBody, CardTitle  } from 'reactstrap';

import './Film.css';

class Film extends Component {
    render() {
        const film = this.props.film;

        const acteurs = [];
        for (const acteur of film.actors) {
            acteurs.push(<li key={ acteurs.length+1 }>{ acteur }</li>)
        }

        return (
            <Card className="Film">
                <CardHeader>
                    <CardTitle className="text-center mb-0" tag="h5">{ film.movie }</CardTitle>
                </CardHeader>
                <CardBody className="d-flex p-0">
                    <div className="poster">
                        <img className="img-fluid" src={ film.poster } alt="poster" />
                    </div>
                    <div className="data">
                        <h6><u>Acteurs :</u></h6>
                        <ul>{ acteurs }</ul>

                        <h6><u>Infos :</u></h6>
                        <table>
                            <tr><th>Durée :</th><td>{ film.duration } min</td></tr>
                            <tr><th>Sorti en :</th><td>{ film.yearOfRelease }</td></tr>
                            { this.formatBoxOffice() }
                            { this.formatRottenTomatoes() }
                        </table>
                    </div>
                </CardBody>
            </Card>
        )
    }

    formatBoxOffice() {
        const bo = this.props.film.boxOffice;

        if (bo !== null) {
            return <tr><th>Box office :</th><td>{ bo.toLocaleString() }$</td></tr>;
        }
    }

    formatRottenTomatoes() {
        const score = this.props.film.rottenTomatoesScore;

        if (score !== null) {
            return <tr><th>Rotten Tomatoes :</th><td>{ this.props.film.rottenTomatoesScore }%</td></tr>
        }
    }
}

export default Film;