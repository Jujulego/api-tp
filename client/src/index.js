import React from 'react';
import ReactDOM from 'react-dom';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

import App from './Components/App';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// Font Awesome
library.add(fas);

ReactDOM.render(<App />, document.getElementById('root'));
