// Importations
import Router from 'express';
import axios from 'axios';

// Constantes
const API_KEY = "b44ba8dd";
const IMDB_URL = "http://www.omdbapi.com/";

// Database
const films = [
    {
        id: "1",
        movie: "Star Wars: Episode IV - A New Hope",
        yearOfRelease: 1977,
        duration: 121,
        actors: [
            "Mark Hamill",
            "Harrison Ford",
            "Carrie Fisher",
            "Peter Cushing"
        ],
        poster: "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
        boxOffice: null,
        rottenTomatoesScore: 93
    },
];

let maxid = 1;
function generate_id() {
    return (++maxid).toString();
}

function find_by_id(id) {
    return films.find((film) => film.id === id);
}

function parse_imdb(data, id = undefined) {
    const film = {
        id: id || generate_id(),
        movie: data.Title,
        yearOfRelease: parseInt(data.Year),
        duration: parseInt(data.Runtime.split(" ")[0]),
        actors: data.Actors.split(", "),
        poster: data.Poster,
        boxOffice: parseInt(data.BoxOffice.slice(1).replace(/,/g, "")) || null,
        rottenTomatoesScore: null,
    };

    const rating = data.Ratings.find((rating) => rating.Source === "Rotten Tomatoes");
    if (rating !== undefined) {
        film.rottenTomatoesScore = parseInt(rating.Value) || null;
    }

    return film;
}

// Routes
const router = new Router();

router.get('/movies', function(req, res, next) {
    res.send(films);
});

router.put('/movie', function(req, res, next) {
    // Gardien
    if (req.body.movie === undefined) {
        res.status(400).send({"error": "missing required parameter 'movie'"});
        return;
    }

    // Ajout
    axios.get(IMDB_URL, {
            params: {
                t: req.body.movie,
                apikey: API_KEY,
            }
        })
        .then(function(rep) {
            if (rep.data.Response === "True") {
                const film = parse_imdb(rep.data);
                films.push(film);

                res.send(film);
            } else {
                res.status(404).send({ "error": rep.data.Error });
            }
        })
        .catch(function(err) {
            console.error(err);
            res.status(500).send({ "error": err })
        });
});

router.route('/movie/:id')
    .get(function(req, res, next) {
        // Recherche
        const film = find_by_id(req.params.id);

        if (film) {
            res.send(film);
        } else {
            // Erreur :
            res.status(404).send({"error": `${req.params.id} not found`});
        }
    })
    .post(function(req, res, next) {
        // Gardien
        if (req.body.movie === undefined) {
            res.status(400).send({"error": "missing required parameter 'movie'"});
            return;
        }

        // Ajout
        axios.get(IMDB_URL, {
                params: {
                    t: req.body.movie,
                    apikey: API_KEY,
                }
            })
            .then(function(rep) {
                if (rep.data.Response === "True") {
                    const film = parse_imdb(rep.data, req.params.id);
                    films[films.indexOf(find_by_id(req.params.id))] = film;

                    res.send(film);
                } else {
                    res.status(404).send({ "error": rep.data.Error });
                }
            })
            .catch(function(err) {
                console.error(err);
                res.status(500).send({ "error": err })
            });
    })
    .delete(function(req, res, next) {
        // Recherche
        const film = find_by_id(req.params.id);

        if (film) {
            films.splice(req.params.id, 1);
            res.send(film);
        } else {
            // Erreur :
            res.status(404).send({"error": `${req.params.id} not found`});
        }
    });

export default router;
